<?php
/**
 * @author   	Copyright (C) 2019 cg@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS 	?>
		<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/petitformalscript-regular-webfont.ttf">
		<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/montserrat-semibold-webfont.ttf">
		<link rel="preload" as="font" crossorigin type="font/ttf" href="/templates/089-standard/fonts/montserrat-regular-webfont.ttf">
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/normalize.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/responsive.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/styles.css"; ?>" rel="stylesheet" type="text/css" />		
</head>
<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<!-- Body -->
		<div id="wrapper" class="fullwidth site_wrapper">
			<div class="above-the-fold">	
			<?php									
				// including menu
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				
				if($this->countModules('header')) : 
					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
				endif;

				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');

				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			?>
			</div>
			<div class="below-the-fold">
			<?php 
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');				
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
			?>
			</div>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript">

		jQuery(window).on('load',()=>{
			jQuery('body').addClass('ready');
		})

		jQuery(document).ready(function() {
			
			<?php if($clientMobile) : ?>
				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse, .navbar').toggleClass('openMenu');
					jQuery('#wrapper').toggleClass('blackened');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
				});
					<?php if(strpos($pageclass, "projekte")) : // nur galerie ?>
						jQuery('h3.gallery--preview--header').on('click', function(e){
								let el = jQuery(this).parent().siblings('div[id^="gallery-modal"]');
								el.css('display', 'block');
								//console.log(el);
							})
					<?php endif;?>
					
			<?php else: //sticky: ?>
				jQuery(window).scroll(function(){
					(jQuery(this).scrollTop() > 113) ? jQuery('.navbar-wrapper').addClass('sticky') : jQuery('.navbar-wrapper').removeClass('sticky');
				});
			<?php endif; ?>

			<?php if(!$clientMobile && $frontpage) : //parallax bckgrImg: ?>
				jQuery(window).scroll(function () {
				    jQuery(".bottom-bckgr-parallax").css("background-position","0% -" + ((jQuery(this).scrollTop() / 16)) + "px");
				});				
			<?php endif;?>

			jQuery('.item-199 a.dropdown-toggle, body.unternehmen .breadcrumb li:nth-child(3) .pathway').click('on', (e)=> {
				e.preventDefault();
				return false;
			})

			jQuery(function(){	 
				jQuery(document).on( 'scroll', function(){
			 
					if (jQuery(window).scrollTop() > 200) {
						jQuery('.scroll-top-wrapper').addClass('show');
					} else {
						jQuery('.scroll-top-wrapper').removeClass('show');
					}
				});	 
				jQuery('.scroll-top-wrapper').on('click', scrollToTop);
			});	 
			function scrollToTop() {
				verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
				element = jQuery('body');
				offset = element.offset();
				offsetTop = offset.top;
				jQuery('html, body').animate({scrollTop: offsetTop}, 650, 'swing');
			}

		});

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>
	<div class="scroll-top-wrapper ">
		<span class="scroll-top-inner">
			<span aria-hidden="true">&#8593;</span>
		</span>
	</div>
</body>
</html>

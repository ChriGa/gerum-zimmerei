<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2019 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="footer-wrap">				
		<?php if ($this->countModules('footnav')) : ?>
			<div class="row-fluid">								
				<div class="span8 footnav">
					<div class="module_footer position_footnav">
						<jdoc:include type="modules" name="footnav" style="custom" />
					</div>			
				</div>
			</div>		
		<?php endif ?>
		<?php if($this->countModules('footer')) : ?>
			<div class="row-fluid">				
				<div class="span12 footer--module">
					<jdoc:include type="modules" name="footer" style="custom" />
				</div>
			</div>
		<?php endif;?>
		<div class="footer--bottom">		
			<div class="logo--small__footer">
				<a class="brand" href="<?php echo $this->baseurl; ?>">
					<?php echo $logo; ?>
					<?php if ($this->params->get('sitedescription')) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
					<?php endif; ?>
				</a>
			</div>
		    <div class="vcard"> 
		        <p class="org">Georg Gerum GmbH<br />Zimmerei - Schreinerei - CNC Abbund</p>
		        <p class="adr"><span class="street-address">Schloßbergerstr. 23 - </span>
		          <span class="postal-code">82290 </span><span class="region">Landsberied</span>
		      	</p>
		        <p class="tel "><a class="" href="tel:+49-814112139">Tel: 08141 / 12139</a></p>
		        <p>
		         	 <a class="email" href="mailto:info@gerum-zimmerei.de">info(at)gerum-zimmerei.de</a>
		        </p>

		    </div>	
	   	</div>	
	</div>
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper innerwidth center">
			<p>&copy; <?php print date("Y") . " " . $sitename; ?> | <a class="imprLink uppercase" href="/impressum.html" title="Impressum Gerum Zimmerei">Impressum</a> | <a class="imprLink uppercase" href="/datenschutz.html">Datenschutz</a></p>
		</div>
	</div>	
</footer>
		
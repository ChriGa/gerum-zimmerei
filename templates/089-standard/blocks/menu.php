<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 

?>
<div class="navbar-wrapper--container">
  <nav class="navbar-wrapper flex fullwidth">
      <div class="logo--small">
        <a class="brand" href="<?php echo $this->baseurl; ?>">
        <?php echo $logo; ?>
          <?php if ($this->params->get('sitedescription')) : ?>
            <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
          <?php endif; ?>
        </a>
        <div class="logo-container--text">
          <h2 class="uppercase null1-15">Georg Gerum Gmbh</h2>
          <h3 class="uppercase null15-16">Zimmerer - Schreinerei - CNC Abbund</h3>
        </div>        
      </div>
      <div class="navbar">
        <div class="navbar--inner">
          <?php print ($detectAgent =="phone ") ? '<button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">' : '<button type="button" class="btn btn-navbar">'; ?>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>                                
        <?php if ($this->countModules('menu')) : ?>
          <div class="nav-collapse collapse "  role="navigation">
            <jdoc:include type="modules" name="menu" style="custom" />
          </div>
        <?php endif; ?>
        </div>
      </div>
  </nav>
</div>